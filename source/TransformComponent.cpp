// TransformComponent.cpp

#include "stdafx.h"
#include "TransformComponent.hpp"

namespace summerproject
{
	TransformComponent::TransformComponent()
	{
	}

	TransformComponent::~TransformComponent()
	{
	}

	EComponentType TransformComponent::GetType() const
	{
		return EComponentType::TransformComponent;
	}

	const sf::Transform& TransformComponent::GetTransform() const
	{
		return m_transformable.getTransform();
	}

	void TransformComponent::SetPosition(const sf::Vector2f& position)
	{
		m_transformable.setPosition(position);
	}
} // namespace summerproject
