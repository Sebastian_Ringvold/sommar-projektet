#include "stdafx.h"
#include "Player.hpp"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "InputManager.hpp"
#include "DrawManager.hpp"
#include "AudioManager.hpp"

namespace summerproject
{
	Player::Player()
	{
		TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();
		sf::Texture* texture = texture_manager->CreateTextureFromFile("../assets/Art/cloud.png");

		texture->setSmooth(true);

		m_sprite->setTexture(*texture);
		sf::Sprite sprite(*texture);
		sprite.setPosition(100.f, 100.f);

		m_x = 128;

		m_active = true;
		m_visible = true;
	}

	Player::~Player()
	{
	}

	void Player::Update(float deltatime)
	{
		if (!m_active)
			return;
		float m_x;
		float m_y;
	}

	bool Player::IsVisible()
	{

		return m_visible;
	}

	float Player::GetTimer()
	{

		return m_timer;
	}
};


