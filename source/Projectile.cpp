#include "stdafx.h"
#include "Projectile.hpp"
#include "ServiceLocator.hpp"
#include "TextureManager.hpp"
#include "InputManager.hpp"
#include "DrawManager.hpp"
//#include "Math.hpp"

namespace summerproject
{
	Projectile::Projectile(const sf::Vector2f& position)
	{
		m_sprite->setPosition(position);

		TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();
		sf::Texture* texture = texture_manager->CreateTextureFromFile("../assets/bullet.png");

		m_sprite->setTexture(*texture);

		m_active = true;
		m_visible = true;
	}

	Projectile::~Projectile()
	{

	}

	void Projectile::Update(float deltatime)
	{
		if (!m_active)
			return;
		m_sprite->move(m_direction * m_speed * deltatime);
	}

	bool Projectile::IsActive()
	{
		return m_active;
	}

	float Projectile::GetTimer()
	{
		return m_timer;
	}
};
