// ComponentTestState.cpp
//
#include "stdafx.h"
//#include "DrawManager.hpp"
//#include "TextureManager.hpp"	
//#include "ServiceLocator.hpp"
//#include "ComponentTestState.hpp"
//
//#include "TransformComponent.hpp"
//#include "VisualComponent.hpp"
//#include "PlayerControllerComponent.hpp"
//#include "Entity.hpp"
//
//namespace summerproject
//{
//	ComponentTestState::ComponentTestState()
//	{
//		m_draw_manager = nullptr;
//		m_player = nullptr;
//	}
//
//	ComponentTestState::~ComponentTestState()
//	{
//	}
//
//	bool ComponentTestState::Enter()
//	{
//		m_draw_manager = ServiceLocator<DrawManager>::GetService();
//
//		TextureManager* texture_manager = ServiceLocator<TextureManager>::GetService();
//		sf::Texture* texture = texture_manager->CreateTextureFromFile("../assets/player.png");
//
//		m_player = new Entity;
//		m_player->AddComponent(new TransformComponent);
//		m_player->AddComponent(new VisualComponent(*texture));
//		m_player->AddComponent(new PlayerControllerComponent);
//
//		return true;
//	}
//
//	void ComponentTestState::Exit()
//	{
//		if (m_player)
//		{
//			delete m_player;
//			m_player = nullptr;
//		}
//	}
//
//	bool ComponentTestState::Update(float deltatime)
//	{
//		return true;
//	}
//
//	void ComponentTestState::Draw()
//	{
//		TransformComponent* transComp = m_player->GetComponent<TransformComponent>(EComponentType::TransformComponent);
//		VisualComponent* visComp = m_player->GetComponent<VisualComponent>(EComponentType::VisualComponent);
//
//		sf::RenderStates states;
//		states.transform = transComp->GetTransform();
//		m_draw_manager->Draw(visComp->GetSprite(), states);
//	}
//
//	std::string ComponentTestState::GetNextState()
//	{
//		return std::string("");
//	}
//} // namespace summerproject
