// PathfindingTestState.cpp

#include "stdafx.h"
#include "DrawManager.hpp"
#include "InputManager.hpp"
#include "ServiceLocator.hpp"
#include "PathfindingTestState.hpp"

namespace summerproject
{
	PathfindingTestState::PathfindingTestState()
	{
		m_draw_manager = nullptr;
		m_screen_width = 1024.0f;
		m_screen_height = 600.0f;
		m_mouse_button = EMouseButton::MOUSE_BUTTON_UNKNOWN;
		m_start_index = -1;
		m_goal_index = -1;

		m_node_color[NODE_TYPE_PASSABLE] = sf::Color::White;
		m_node_color[NODE_TYPE_NON_PASSABLE] = sf::Color::Red;
		m_node_color[NODE_TYPE_PATH] = sf::Color::Cyan;
		m_node_color[NODE_TYPE_START] = sf::Color::Blue;
		m_node_color[NODE_TYPE_GOAL] = sf::Color::Green;

		for (unsigned int index = 0; index < (unsigned int)EActionType::Count; index++)
			m_action[index] = false;
	}

	PathfindingTestState::~PathfindingTestState()
	{
		m_draw_manager = nullptr;
	}

	bool PathfindingTestState::Enter()
	{
		InputManager* input_manager = ServiceLocator<InputManager>::GetService();
		
		// note(tommi): listen to actions
		input_manager->RegisterKeyActionListener(EActionType::Left, this, &PathfindingTestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::Right, this, &PathfindingTestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::Up, this, &PathfindingTestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::Down, this, &PathfindingTestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::Fire, this, &PathfindingTestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::Select1, this, &PathfindingTestState::OnAction);
		input_manager->RegisterKeyActionListener(EActionType::Select2, this, &PathfindingTestState::OnAction);

		// note(tommi): listen to mouse events
		input_manager->RegisterMouseEventListener(this, &PathfindingTestState::OnMouse);

		// note(tommi): save the draw manager for a rainy day
		m_draw_manager = ServiceLocator<DrawManager>::GetService();

		// settings for the grid
		m_grid_width = 16;
		m_grid_height = 12;
		m_grid_nodes.resize(m_grid_width * m_grid_height);
		m_node_square = 40.0f;
		m_node_offset_x = (m_screen_width - (m_grid_width * m_node_square)) * 0.5f;
		m_node_offset_y = (m_screen_height - (m_grid_height * m_node_square)) * 0.5f;

		// note(tommi): multiply by 4 since we use quads for nodes
		m_node_vertex_array.setPrimitiveType(sf::Quads);
		m_node_vertex_array.resize(m_grid_nodes.size() * 4);

		for (unsigned int index = 0, idx = 0; index < m_grid_nodes.size(); index++)
		{
			float x = static_cast<float>(index % m_grid_width);
			float y = static_cast<float>(index / m_grid_width);

			sf::Color color = m_node_color[NODE_TYPE_PASSABLE];

			m_node_vertex_array[idx].position.x = m_node_offset_x + m_node_square * x;
			m_node_vertex_array[idx].position.y = m_node_offset_y + m_node_square * y;
			m_node_vertex_array[idx].color = color;
			idx++;

			m_node_vertex_array[idx].position.x = m_node_offset_x + m_node_square * x + m_node_square;
			m_node_vertex_array[idx].position.y = m_node_offset_y + m_node_square * y;
			m_node_vertex_array[idx].color = color;
			idx++;

			m_node_vertex_array[idx].position.x = m_node_offset_x + m_node_square * x + m_node_square;
			m_node_vertex_array[idx].position.y = m_node_offset_y + m_node_square * y + m_node_square;
			m_node_vertex_array[idx].color = color;
			idx++;

			m_node_vertex_array[idx].position.x = m_node_offset_x + m_node_square * x;
			m_node_vertex_array[idx].position.y = m_node_offset_y + m_node_square * y + m_node_square;
			m_node_vertex_array[idx].color = color;
			idx++;
		}

		// note(tommi): overlay grid
		m_grid_vertex_array.setPrimitiveType(sf::Lines);
		m_grid_vertex_array.resize((m_grid_width + m_grid_height + 2) * 2);
		for (unsigned int index = 0, idx = 0; index < m_grid_width + 1; index++)
		{
			float x = m_node_offset_x + m_node_square * static_cast<float>(index);

			float minY = m_node_offset_y;
			float maxY = m_node_offset_y + m_node_square * static_cast<float>(m_grid_height);

			m_grid_vertex_array[idx].position.x = x;
			m_grid_vertex_array[idx].position.y = minY;
			m_grid_vertex_array[idx].color = sf::Color::Black;
			idx++;

			m_grid_vertex_array[idx].position.x = x;
			m_grid_vertex_array[idx].position.y = maxY;
			m_grid_vertex_array[idx].color = sf::Color::Black;
			idx++;
		}

		for (unsigned int index = 0, idx = (m_grid_width + 1) * 2; index < m_grid_height + 1; index++)
		{
			float minX = m_node_offset_x;
			float maxX = minX + m_node_square * static_cast<float>(m_grid_width);

			float y = m_node_offset_y + m_node_square * static_cast<float>(index);

			m_grid_vertex_array[idx].position.x = minX;
			m_grid_vertex_array[idx].position.y = y;
			m_grid_vertex_array[idx].color = sf::Color::Black;
			idx++;

			m_grid_vertex_array[idx].position.x = maxX;
			m_grid_vertex_array[idx].position.y = y;
			m_grid_vertex_array[idx].color = sf::Color::Black;
			idx++;
		}

		ClearNodes();

		return true;
	}

	void PathfindingTestState::Exit()
	{
		InputManager* input_manager = ServiceLocator<InputManager>::GetService();

		// note(tommi): stop listening to actions
		input_manager->UnregisterKeyActionListener(EActionType::Left, this);
		input_manager->UnregisterKeyActionListener(EActionType::Right, this);
		input_manager->UnregisterKeyActionListener(EActionType::Up, this);
		input_manager->UnregisterKeyActionListener(EActionType::Down, this);
		input_manager->UnregisterKeyActionListener(EActionType::Fire, this);
		input_manager->UnregisterKeyActionListener(EActionType::Select1, this);
		input_manager->UnregisterKeyActionListener(EActionType::Select2, this);

		// note(tommi): stop listening to mouse events
		input_manager->UnregisterMouseEventListener(this);
	}

	bool PathfindingTestState::Update(float deltatime)
	{
		if (m_action[(unsigned int)EActionType::Select1])
		{
			m_action[(unsigned int)EActionType::Select1] = false;
			ResetStartAndGoal();
		}
		else if (m_action[(unsigned int)EActionType::Select2])
		{
			m_action[(unsigned int)EActionType::Select2] = false;
			ClearNodes();
		}

		if (m_mouse_button != EMouseButton::MOUSE_BUTTON_UNKNOWN)
		{
			unsigned int x = ((m_mouse_up.x - static_cast<unsigned short>(m_node_offset_x)) / static_cast<unsigned short>(m_node_square));
			unsigned int y = (m_mouse_up.y - static_cast<unsigned short>(m_node_offset_y)) / static_cast<unsigned short>(m_node_square);
			unsigned int index = y * m_grid_width + x;

			if (x < m_grid_width && y < m_grid_height)
			{
				if (m_mouse_button == EMouseButton::MOUSE_BUTTON_LEFT)
				{
					// note(tommi): passable / non-passable
					if (m_grid_nodes[index].m_type == NODE_TYPE_PASSABLE)
						m_grid_nodes[index].m_type = NODE_TYPE_NON_PASSABLE;
					else if (m_grid_nodes[index].m_type == NODE_TYPE_NON_PASSABLE)
						m_grid_nodes[index].m_type = NODE_TYPE_PASSABLE;
					else
						m_grid_nodes[index].m_type = NODE_TYPE_PASSABLE;

					UpdateNodeColor(index);
				}
				else if (m_mouse_button == EMouseButton::MOUSE_BUTTON_RIGHT)
				{
					// note(tommi): start / goal
					if (m_grid_nodes[index].m_type == NODE_TYPE_PASSABLE)
					{
						if (m_start_index == -1)
						{
							m_start_index = index;
							m_grid_nodes[index].m_type = NODE_TYPE_START;
						}
						else if (m_goal_index == -1)
						{
							m_goal_index = index;
							m_grid_nodes[index].m_type = NODE_TYPE_GOAL;
						}
					}
					else
					{
						if (m_grid_nodes[index].m_type == NODE_TYPE_START)
							m_start_index = -1;
						else if (m_grid_nodes[index].m_type == NODE_TYPE_GOAL)
							m_goal_index = -1;
						m_grid_nodes[index].m_type = NODE_TYPE_PASSABLE;
					}

					UpdateNodeColor(index);
				}
			}
			m_mouse_button = EMouseButton::MOUSE_BUTTON_UNKNOWN;
		}

		if (m_start_index != -1 && m_goal_index != -1 && m_action[(unsigned int)EActionType::Fire])
		{
			m_action[(unsigned int)EActionType::Fire] = false;
			SearchPath();
		}

		return true;
	}

	void PathfindingTestState::Draw()
	{
		m_draw_manager->Draw(m_node_vertex_array);
		m_draw_manager->Draw(m_grid_vertex_array);
	}

	std::string PathfindingTestState::GetNextState()
	{
		return std::string("");
	}

	// 	private
	void PathfindingTestState::OnAction(EActionType action, bool state)
	{
		m_action[(unsigned int)action] = state;
	}

	void PathfindingTestState::OnMouse(const MouseEvent& event)
	{
		if (event.GetType() == EMouseEventType::MOUSE_EVENT_UP)
		{
			m_mouse_up.x = event.GetX();
			m_mouse_up.y = event.GetY();
			m_mouse_button = event.GetButton();
		}
	}

	void PathfindingTestState::UpdateNodeColor(unsigned int index)
	{
		sf::Color color = m_node_color[m_grid_nodes[index].m_type];

		sf::Vertex* vertices = &m_node_vertex_array[index * 4];
		for (unsigned int idx = 0; idx < 4; idx++)
			vertices[idx].color = color;
	}

	// note(tommi): here we start our path finding
	void PathfindingTestState::SearchPath()
	{
		const float localScores[8] = { 1.4f, 1.0f, 1.4f, 1.0f, 1.0f, 1.4f, 1.0f, 1.4f };

		m_open_list.clear();
		m_closed_list.clear();

		AddNodeToList(m_open_list, &m_grid_nodes[m_start_index]);

		bool goalReached = false;
		while (!goalReached)
		{
			GridNode* node = FindLowestScoreInOpenList();
			if (!node)
				break;

			RemoveNodeFromList(m_open_list, node);

			// get adj. nodes
			GridNode* adj[8] =
			{
				GetNodeAt(node->m_x - 1, node->m_y - 1),
				GetNodeAt(node->m_x, node->m_y - 1),
				GetNodeAt(node->m_x + 1, node->m_y - 1),

				GetNodeAt(node->m_x - 1, node->m_y),
				GetNodeAt(node->m_x + 1, node->m_y),

				GetNodeAt(node->m_x - 1, node->m_y + 1),
				GetNodeAt(node->m_x, node->m_y + 1),
				GetNodeAt(node->m_x + 1, node->m_y + 1)
			};

			for (unsigned int i = 0; i < 8; i++)
			{
				if (adj[i] != nullptr)				
				{
					// we found the goal
					if (adj[i] == &m_grid_nodes[m_goal_index])
					{
						adj[i]->m_parent = node;
						AddNodeToList(m_closed_list, adj[i]);
						goto path_found;
					}

					// checkin if the adjacent node exist in open or closed list
					bool isOpen = IsNodeInList(m_open_list, adj[i]);
					bool isClosed = IsNodeInList(m_closed_list, adj[i]);
					if (isClosed)
						continue;

					float tentative = node->m_G + localScores[i] * 5.0f;
					if (!isOpen || adj[i]->m_G < node->m_G)
					{
						adj[i]->m_parent = node;
						adj[i]->m_G = tentative;
						adj[i]->m_H = CalculateHeuristics(adj[i]->m_x, adj[i]->m_y);
						adj[i]->m_F = adj[i]->m_G + adj[i]->m_H;

						if (!isOpen)
							AddNodeToList(m_open_list, adj[i]);
					}
				}
			}
			AddNodeToList(m_closed_list, node);
		}

	path_found:
		GridNode* node = m_closed_list.back();
		while (node->m_parent)
		{
			// change type
			node->m_type = NODE_TYPE_PATH;
			// update color
			UpdateNodeColor(node->m_y * m_grid_width + node->m_x);
			// next
			node = node->m_parent;
		}
	}

	void PathfindingTestState::AddNodeToList(std::vector<GridNode*>& list, GridNode* node)
	{
		list.push_back(node);
	}

	void PathfindingTestState::RemoveNodeFromList(std::vector<GridNode*>& list, GridNode* node)
	{
		auto itr = list.begin();
		while (itr != list.end())
		{
			if ((*itr) == node)
			{
				list.erase(itr);
				break;
			}
			++itr;
		}
	}

	bool PathfindingTestState::IsNodeInList(const std::vector<PathfindingTestState::GridNode*>& list, PathfindingTestState::GridNode* node)
	{
		for (unsigned int index = 0; index < list.size(); index++)
		{
			if (list[index] == node)
				return true;
		}
		return false;
	}

	PathfindingTestState::GridNode* PathfindingTestState::FindLowestScoreInOpenList()
	{
		if (m_open_list.empty())
			return nullptr;
 
		int index = 0;
		for (unsigned int i = 1; i < m_open_list.size(); i++)
		{
			if (m_open_list[i]->m_F < m_open_list[index]->m_F)
				index = i;
		}
		return m_open_list[index];
	}

	PathfindingTestState::GridNode* PathfindingTestState::GetNodeAt(unsigned int x, unsigned int y)
	{
		if (x >= m_grid_width || y >= m_grid_height)
			return nullptr;

		unsigned int index = y * m_grid_width + x;
		if (m_grid_nodes[index].m_type != NODE_TYPE_NON_PASSABLE)
			return &m_grid_nodes[index];
		return nullptr;
	}

	float PathfindingTestState::CalculateHeuristics(int x, int y)
	{
		GridNode* goal = &m_grid_nodes[m_goal_index];
		float dx = static_cast<float>(x - static_cast<int>(goal->m_x));
		float dy = static_cast<float>(y - static_cast<int>(goal->m_y));
		// manhattan distance
		return fabs(dx) + fabs(dy);
	}

	void PathfindingTestState::ClearNodes()
	{
		for (unsigned int index = 0; index < m_grid_nodes.size(); index++)
		{
			GridNode& node = m_grid_nodes[index];
			node.m_type = NODE_TYPE_PASSABLE;
			node.m_x = index % m_grid_width;
			node.m_y = index / m_grid_width;
			node.m_F = 0.0f;
			node.m_G = 0.0f;
			node.m_H = 0.0f;
			node.m_parent = nullptr;
			UpdateNodeColor(index);
		}
	}

	void PathfindingTestState::ResetStartAndGoal()
	{
		if (m_start_index != -1)
		{
			m_grid_nodes[m_start_index].m_type = NODE_TYPE_PASSABLE;
			UpdateNodeColor(m_start_index);
		}
		if (m_goal_index != -1)
		{
			m_grid_nodes[m_goal_index].m_type = NODE_TYPE_PASSABLE;
			UpdateNodeColor(m_goal_index);
		}

		m_start_index = -1;
		m_goal_index = -1;

		for (unsigned int index = 0; index < m_grid_nodes.size(); index++)
		{
			GridNode& node = m_grid_nodes[index];
			if (node.m_type == NODE_TYPE_PATH)
			{
				node.m_type = NODE_TYPE_PASSABLE;
				UpdateNodeColor(index);
			}
			node.m_F = 0.0f;
			node.m_G = 0.0f;
			node.m_H = 0.0f;
			node.m_parent = nullptr;
		}
	}
} // namespace summerproject
