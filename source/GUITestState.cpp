// GUITestState.cpp

#include "stdafx.h"
#include "DrawManager.hpp"
#include "InputManager.hpp"
#include "TextureManager.hpp"
#include "FontManager.hpp"
#include "ServiceLocator.hpp"
#include "GUIManager.hpp"
#include "GUITestState.hpp"

#include "GUIWidget.hpp"
#include "GUILabel.hpp"

namespace summerproject
{
	GUITestState::GUITestState()
	{
		m_draw_manager = nullptr;
		m_gui_manager = nullptr;
	}

	GUITestState::~GUITestState()
	{
		m_draw_manager = nullptr;
	}

	bool GUITestState::Enter()
	{
		m_draw_manager = ServiceLocator<DrawManager>::GetService();

		FontManager* font_manager = ServiceLocator<FontManager>::GetService();
		sf::Font* font = font_manager->CreateFontFromFile("../assets/font/eightbitmadness.ttf");

		// gui label
		GUILabel* label = new GUILabel(font);
		label->SetTextString("Test");
		label->SetTextColor(sf::Color::White);
		label->SetRect(sf::Rect<float>(10.0f, 10.0f, 0.0f, 0.0f));

		m_gui_manager = new GUIManager;
		m_gui_manager->AttachWidget(label);

		return true;
	}

	void GUITestState::Exit()
	{
		if (m_gui_manager)
		{
			delete m_gui_manager;
			m_gui_manager = nullptr;
		}
	}

	bool GUITestState::Update(float deltatime)
	{
		m_gui_manager->Update();

		return true;
	}

	void GUITestState::Draw()
	{
		m_gui_manager->Draw(m_draw_manager);
	}

	std::string GUITestState::GetNextState()
	{
		return std::string("");
	}

	// private
	void GUITestState::OnAction(EActionType action, bool state)
	{
	}

	void GUITestState::OnMouse(const MouseEvent& event)
	{
	}
} // namespace summerproject
