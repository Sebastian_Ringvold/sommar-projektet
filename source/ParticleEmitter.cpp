// ParticleEmitter.cpp

#include "stdafx.h"
#include "ParticleEmitter.hpp"

namespace summerproject
{
	ParticleEmitter::ParticleEmitter()
	{
		m_texture = nullptr;
	}

	ParticleEmitter::~ParticleEmitter()
	{
	}

	const sf::Vector2f& ParticleEmitter::GetPosition() const
	{
		return m_position;
	}

	void ParticleEmitter::SetPosition(const sf::Vector2f& position)
	{
		m_position = position;
	}
} // namespace summerproject
