// MouseEvent.cpp

#include "stdafx.h"
#include "MouseEvent.hpp"

namespace summerproject
{
	MouseEvent::MouseEvent()
	{
		m_type = EMouseEventType::MOUSE_EVENT_UNKNOWN;
	}

	MouseEvent::MouseEvent(EMouseEventType type)
	{
		m_type = type;
	}

	const EMouseEventType MouseEvent::GetType() const
	{
		return m_type;
	}

	const float MouseEvent::GetDelta() const
	{
		return m_delta;
	}

	const unsigned short MouseEvent::GetX() const
	{
		return m_x;
	}

	const unsigned short MouseEvent::GetY() const
	{
		return m_y;
	}

	const EMouseButton MouseEvent::GetButton() const
	{
		return m_button;
	}

	const bool MouseEvent::GetButtonState() const
	{
		return m_state;
	}
} // namespace summerproject
