// CollisionShapes.cpp

#include "stdafx.h"
#include "CollisionShapes.hpp"

namespace summerproject
{
	AxisAlignedBoundingBox::AxisAlignedBoundingBox()
	{
	}

	AxisAlignedBoundingBox::AxisAlignedBoundingBox(const sf::Vector2f& center, const sf::Vector2f& extent)
	{
		m_center = center;
		m_extent = extent;
	}

	const sf::Vector2f& AxisAlignedBoundingBox::GetCenter() const
	{
		return m_center;
	}

	void AxisAlignedBoundingBox::SetCenter(const sf::Vector2f& center)
	{
		m_center = center;
	}

	const sf::Vector2f& AxisAlignedBoundingBox::GetExtent() const
	{
		return m_extent;
	}

	void AxisAlignedBoundingBox::SetExtent(const sf::Vector2f& extent)
	{
		m_extent = extent;
	}

	BoundingCircle::BoundingCircle()
	{
	}

	BoundingCircle::BoundingCircle(const sf::Vector2f& center, float radius)
	{
		m_center = center;
		m_radius = radius;
	}

	const sf::Vector2f& BoundingCircle::GetCenter() const
	{
		return m_center;
	}

	void BoundingCircle::SetCenter(const sf::Vector2f& center)
	{
		m_center = center;
	}

	float BoundingCircle::GetRadius() const
	{
		return m_radius;
	}

	void BoundingCircle::SetRadius(float radius)
	{
		m_radius = radius;
	}

	ObjectOrientedBoundingBox::ObjectOrientedBoundingBox()
	{
	}

	ObjectOrientedBoundingBox::ObjectOrientedBoundingBox(const sf::Vector2f& center, const sf::Vector2f& extent, float radians)
	{
		m_center = center;
		m_extent = extent;
		m_radians = radians;
		RecalculateCorners();
	}

	const sf::Vector2f& ObjectOrientedBoundingBox::GetCenter() const
	{
		return m_center;
	}

	void ObjectOrientedBoundingBox::SetCenter(const sf::Vector2f& center)
	{
		m_center = center;
	}

	const sf::Vector2f& ObjectOrientedBoundingBox::GetExtent() const
	{
		return m_extent;
	}

	void ObjectOrientedBoundingBox::SetExtent(const sf::Vector2f& extent)
	{
		m_extent = extent;
		RecalculateCorners();
	}

	float ObjectOrientedBoundingBox::GetRadians() const
	{
		return m_radians;
	}

	void ObjectOrientedBoundingBox::SetRadians(float radians)
	{
		m_radians = radians;
		RecalculateCorners();
	}

	const sf::Vector2f& ObjectOrientedBoundingBox::GetCorner(unsigned int index) const
	{
		assert(index < 4);
		return m_corners[index] + m_center;
	}

	void ObjectOrientedBoundingBox::RecalculateCorners()
	{
		const float fC = cosf(m_radians);
		const float fS = sinf(m_radians);

		m_corners[0].x = -m_extent.x;
		m_corners[0].y = -m_extent.y;
		m_corners[1].x =  m_extent.x;
		m_corners[1].y = -m_extent.y;
		m_corners[2].x =  m_extent.x;
		m_corners[2].y =  m_extent.y;
		m_corners[3].x = -m_extent.x;
		m_corners[3].y =  m_extent.y;

		for (unsigned int index = 0; index < 4; index++)
		{
			m_corners[index].x = m_corners[index].x * fC - m_corners[index].y * fS;
			m_corners[index].y = m_corners[index].x * fS + m_corners[index].y * fC;
		}
	}

	LineSegment::LineSegment()
	{
	}

	LineSegment::LineSegment(const sf::Vector2f& start, const sf::Vector2f& end)
	{
		m_start = start;
		m_end = end;
	}

	const sf::Vector2f& LineSegment::GetStart() const
	{
		return m_start;
	}

	void LineSegment::SetStart(const sf::Vector2f& start)
	{
		m_start = start;
	}

	const sf::Vector2f& LineSegment::GetEnd() const
	{
		return m_end;
	}

	void LineSegment::SetEnd(const sf::Vector2f& end)
	{
		m_end = end;
	}

	// overlaps
	// warning(tommi): these are not tested properly!
	bool Overlap(const AxisAlignedBoundingBox& lhs, const AxisAlignedBoundingBox& rhs, sf::Vector2f& overlap)
	{
		sf::Vector2f lhsMin = lhs.GetCenter() - lhs.GetExtent();
		sf::Vector2f lhsMax = lhs.GetCenter() + lhs.GetExtent();
		sf::Vector2f rhsMin = rhs.GetCenter() - rhs.GetExtent();
		sf::Vector2f rhsMax = rhs.GetCenter() + rhs.GetExtent();

		float deltaCenterX = lhs.GetCenter().x - rhs.GetCenter().x;
		float extentX = lhs.GetExtent().x + rhs.GetExtent().x;
		if (fabs(deltaCenterX) < extentX)
		{
			float deltaCenterY = lhs.GetCenter().y - rhs.GetCenter().y;
			float extentY = lhs.GetExtent().y + rhs.GetExtent().y;
			if (fabs(deltaCenterY) < extentY)
			{
				float deltaX = deltaCenterX - extentX;
				float deltaY = deltaCenterY - extentY;
				if (deltaX > deltaY)
				{
					overlap.y = deltaCenterY < 0.0f ? -deltaY : deltaY;
				}
				else if (deltaX < deltaY)
				{
					overlap.x = deltaCenterX < 0.0f ? -deltaX : deltaX;
				}
				else
				{
					overlap.x = deltaCenterX < 0.0f ? -deltaX : deltaX;
					overlap.y = deltaCenterY < 0.0f ? -deltaY : deltaY;
				}
				return true;
			}
		}

		return false;
	}

	bool Overlap(const AxisAlignedBoundingBox& lhs, const ObjectOrientedBoundingBox& rhs, sf::Vector2f& overlap)
	{
		return false;
	}

	bool Overlap(const AxisAlignedBoundingBox& lhs, const BoundingCircle& rhs, sf::Vector2f& overlap)
	{
		return false;
	}

	bool Overlap(const ObjectOrientedBoundingBox& lhs, const ObjectOrientedBoundingBox& rhs, sf::Vector2f& overlap)
	{
		return false;
	}

	bool Overlap(const ObjectOrientedBoundingBox& lhs, const BoundingCircle& rhs, sf::Vector2f& overlap)
	{
		return false;
	}

	bool Overlap(const LineSegment& lhs, const LineSegment& rhs)
	{
		sf::Vector2f les = lhs.GetEnd() - lhs.GetStart();
		sf::Vector2f rtle = rhs.GetEnd() - lhs.GetStart();
		sf::Vector2f rtls = rhs.GetStart() - lhs.GetStart();
		bool re = Cross(rtle, les) > 0;
		bool rs = Cross(rtls, les) > 0;
		if (re == rs)
			return false;

		sf::Vector2f res = rhs.GetEnd() - rhs.GetStart();
		sf::Vector2f ltre = lhs.GetEnd() - rhs.GetStart();
		sf::Vector2f ltrs = lhs.GetStart() - rhs.GetStart();
		bool le = Cross(ltre, res) > 0;
		bool ls = Cross(ltrs, res) > 0;
		if (le == ls)
			return false;

		return true;
	}
} // namespace summerproject
