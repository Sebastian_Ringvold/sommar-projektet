// Entity.cpp

#include "stdafx.h"
#include "Entity.hpp"

namespace summerproject
{

	Entity::Entity()
	{
		m_sprite = std::make_unique<sf::Sprite>();
	}

	bool Entity::isActive()
	{
		return m_active;
	}

	sf::Sprite* Entity::getSprite() const
	{
		return m_sprite.get();
	}

	sf::Vector2f Entity::getDirection() const
	{
		return m_direction;
	}

	bool Entity::IsVisible()
	{
		return m_visible;
	}

	void Entity::setInvisible()
	{
		m_visible = false;
	}

	void Entity::setVisible(bool visible)
	{
		m_visible = visible;
	}

	float Entity::getSpeed() const
	{
		return m_speed;
	}

	void Entity::setSpeed(float speed)
	{
		m_speed = speed;
	}

	void Entity::setDirection(float x, float y)
	{
		m_direction.x = x;
		m_direction.y = y;
		Normalize(m_direction);
	}

	void Entity::setDirection(const sf::Vector2f& direction)
	{
		m_direction = direction;
	}

	void Entity::setActive(bool active)
	{
		m_active = active;
	}

} // namespace summerproject
