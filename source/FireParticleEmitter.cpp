// FireParticleEmitter.cpp

#include "stdafx.h"
#include "DrawManager.hpp"
#include "TextureManager.hpp"
#include "ServiceLocator.hpp"
#include "FireParticleEmitter.hpp"

namespace summerproject
{
	FireParticleEmitter::FireParticleEmitter(sf::Texture* texture)
	{
		m_texture = texture;

		m_spawn_rate = 1.0f / 20.0f;
		m_spawn_timer = 0.0f;
		m_max_age = 3.0f;
		m_particle_speed = 60.0f;
		m_particle_count = 0;
		for (unsigned int index = 0; index < MAX_PARTICLE_COUNT; index++)
		{
			m_ages[index] = 0.0f;
		}

		m_vertex_array.setPrimitiveType(sf::Quads);
		m_draw_manager = ServiceLocator<DrawManager>::GetService();
	}

	FireParticleEmitter::~FireParticleEmitter()
	{
		m_draw_manager = nullptr;
	}

	void FireParticleEmitter::Update(float deltatime)
	{
		m_spawn_timer += deltatime;
		if (m_spawn_timer > m_spawn_rate)
		{
			m_spawn_timer -= m_spawn_rate;
			SpawnParticle();
		}

		sf::Vector2f particleSize = sf::Vector2f((float)m_texture->getSize().x, (float)m_texture->getSize().y);
		sf::Vector2f halfSize = particleSize * 0.5f;

		m_vertex_array.clear();
		for (unsigned int index = 0; index < m_particle_count; index++)
		{
			m_ages[index] += deltatime;
			if (m_ages[index] > m_max_age)
			{
				DespawnParticle(index);
				continue;
			}

			m_transforms[index].m_position += m_transforms[index].m_direction * m_particle_speed * deltatime;

			sf::Vertex vertices[4];
			vertices[0].position.x = m_transforms[index].m_position.x - halfSize.x;
			vertices[0].position.y = m_transforms[index].m_position.y - halfSize.y;
			vertices[0].texCoords.x = 0.0f;
			vertices[0].texCoords.y = 0.0f;

			vertices[1].position.x = m_transforms[index].m_position.x + halfSize.x;
			vertices[1].position.y = m_transforms[index].m_position.y - halfSize.y;
			vertices[1].texCoords.x = particleSize.x;
			vertices[1].texCoords.y = 0.0f;

			vertices[2].position.x = m_transforms[index].m_position.x + halfSize.x;
			vertices[2].position.y = m_transforms[index].m_position.y + halfSize.y;
			vertices[2].texCoords.x = particleSize.x;
			vertices[2].texCoords.y = particleSize.y;

			vertices[3].position.x = m_transforms[index].m_position.x - halfSize.x;
			vertices[3].position.y = m_transforms[index].m_position.y + halfSize.y;
			vertices[3].texCoords.x = 0.0f;
			vertices[3].texCoords.y = particleSize.y;

			for (unsigned int k = 0; k < 4; k++)
				m_vertex_array.append(vertices[k]);
		}
	}

	void FireParticleEmitter::Draw()
	{
		sf::RenderStates states;
		states.texture = m_texture;
		states.transform.translate(m_position);
		m_draw_manager->Draw(m_vertex_array, states);
	}

	// private
	void FireParticleEmitter::SpawnParticle()
	{
		if (m_particle_count >= MAX_PARTICLE_COUNT)
			return;

		unsigned int index = m_particle_count++;
		m_ages[index] = 0.0f;
		m_transforms[index].m_direction.x = Random(-1.0f, 1.0f);
		m_transforms[index].m_direction.y = -5.0f;
		Normalize(m_transforms[index].m_direction);
		m_transforms[index].m_position = m_position;
	}

	void FireParticleEmitter::DespawnParticle(unsigned int index)
	{
		if (m_particle_count == 0)
			return;
	
		if (m_particle_count == 1)
			m_particle_count--;
		else if ((m_particle_count - 1) == index)
			m_particle_count--;
		else
			std::swap(m_transforms[index], m_transforms[--m_particle_count]);
	}
} // namespace summerproject
