// AbstractState.cpp

#include "stdafx.h"
#include "AbstractState.hpp"

namespace summerproject
{
	// note(tommi): we enable derived classes' destructors 
	// to be called
	AbstractState::~AbstractState()
	{
	}
} // namespace summerproject
