// main.cpp

#include "stdafx.h"
#include "Engine.hpp"

int main(int argc, char* argv[])
{
	summerproject::Engine engine;
	if (engine.Initialize())
		engine.Run();
	engine.Shutdown();
	return 0;
}
