// GUIWidget.cpp

#include "stdafx.h"
#include "GUIWidget.hpp"

namespace summerproject
{
	GUIWidget::AbstractWidgetCallback::~AbstractWidgetCallback()
	{
	}

	GUIWidget::GUIWidget(EWidgetType type)
	{
		m_type = type;
		m_visible = true;
		m_has_text = false;
	}

	GUIWidget::~GUIWidget()
	{
		auto itr = m_callbacks.begin();
		while (itr != m_callbacks.end())
		{
			delete (*itr);
			++itr;
		}
		m_callbacks.clear();
	}

	EWidgetType GUIWidget::GetType() const
	{
		return m_type;
	}

	bool GUIWidget::IsVisible() const
	{
		return m_visible;
	}

	bool GUIWidget::HasText() const
	{
		return m_has_text;
	}

	const sf::Rect<float>& GUIWidget::GetRect() const
	{
		return m_rect;
	}

	void GUIWidget::SetRect(const sf::Rect<float>& rect)
	{
		m_rect = rect;
	}

	void GUIWidget::UnregisterWidgetCallback(void* object)
	{
		auto itr = m_callbacks.begin();
		while (itr != m_callbacks.end())
		{
			if ((*itr)->IsObject(object))
			{
				delete (*itr);
				m_callbacks.erase(itr);
				break;
			}
			++itr;
		}
	}

	// protected
	void GUIWidget::Notify()
	{
		if (m_callbacks.empty())
			return;

		auto itr = m_callbacks.begin();
		while (itr != m_callbacks.end())
		{
			(*itr)->OnChange(this);
			++itr;
		}
	}
} // namespace summerproject
