// ParticleEffectManager.cpp

#include "stdafx.h"
#include "ParticleEffect.hpp"
#include "ParticleEffectManager.hpp"

namespace summerproject
{
	ParticleEffectManager::ParticleEffectManager()
	{
	}

	ParticleEffectManager::~ParticleEffectManager()
	{
		auto itr = m_particle_effects.begin();
		while (itr != m_particle_effects.end())
		{
			delete (*itr);
			++itr;
		}
		m_particle_effects.clear();
	}

	void ParticleEffectManager::Update(float deltatime)
	{
		for (unsigned int index = 0; index < m_particle_effects.size(); index++)
		{
			m_particle_effects[index]->Update(deltatime);
		}
	}

	void ParticleEffectManager::Draw()
	{
		for (unsigned int index = 0; index < m_particle_effects.size(); index++)
		{
			m_particle_effects[index]->Draw();
		}
	}

	void ParticleEffectManager::AttachParticleEffect(ParticleEffect* effect)
	{
		m_particle_effects.push_back(effect);
	}

	void ParticleEffectManager::DetachParticleEffect(ParticleEffect* effect)
	{
		auto itr = m_particle_effects.begin();
		while (itr != m_particle_effects.end())
		{
			if ((*itr) == effect)
			{
				m_particle_effects.erase(itr);
				break;
			}
			++itr;
		}
	}
} // namespace summerproject
