// FontManager.cpp

#include "stdafx.h"
#include "FontManager.hpp"

namespace summerproject
{
	FontManager::FontManager()
	{
	}

	FontManager::~FontManager()
	{
		auto itrFont = m_fonts.begin();
		while (itrFont != m_fonts.end())
		{
			delete itrFont->second;
			++itrFont;
		}
		m_fonts.clear();
	}

	sf::Font* FontManager::CreateFontFromFile(const std::string& filename)
	{
		auto itr = m_fonts.find(filename);
		if (itr == m_fonts.end())
		{
			sf::Font* font = new sf::Font;
			if (!font->loadFromFile(filename))
			{
				// note(tommi): font not found
				delete font;
				assert(false);
			}
			m_fonts.insert(std::pair<std::string, sf::Font*>(filename, font));
			itr = m_fonts.find(filename);
		}
		return itr->second;
	} 
} // namespace summerproject
