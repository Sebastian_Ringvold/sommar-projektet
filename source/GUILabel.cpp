// GUILabel.cpp

#include "stdafx.h"
#include "GUILabel.hpp"

namespace summerproject
{
	GUILabel::GUILabel(sf::Font* font)
		: GUIWidget(EWidgetType::Label)
	{
		m_has_text = true;
		m_text.setFont(*font);
	}

	GUILabel::~GUILabel()
	{
	}

	void GUILabel::OnDraw(sf::VertexArray& vertex_array)
	{
		// void
	}

	void GUILabel::SetTextString(const std::string& text)
	{
		m_text.setString(text);
	}

	void GUILabel::SetTextColor(const sf::Color& color)
	{
		m_text.setColor(color);
	}

	void GUILabel::SetTextSize(unsigned int size)
	{
		m_text.setCharacterSize(size);
	}

	void GUILabel::SetTextPosition(float x, float y)
	{
		m_rect.left = x;
		m_rect.top = y;
		m_text.setPosition(x, y);
	}

	void GUILabel::SetTextPosition(const sf::Vector2f& position)
	{
		m_rect.left = position.x;
		m_rect.top = position.y;
		m_text.setPosition(position.x, position.y);
	}

	const sf::Text& GUILabel::GetText() const
	{
		return m_text;
	}
} // namespace summerproject
