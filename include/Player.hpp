#pragma once

#include "Entity.hpp"
namespace summerproject
{
	class Player : public Entity
	{
	public:

		Player();
		~Player();

		void Update(float deltatime);
		float GetX();
		float GetY();
		bool IsVisible();
		bool IsActive();
		float GetTimer();
		void SetInvisible();
		void Activate();
		void DeActivate();
		void setActive(bool active);

	protected:
		float m_timer;
		bool m_visible;
		bool m_active;
		float m_spawntimer;
		int m_x;
	};
};
