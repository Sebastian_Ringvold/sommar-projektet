// GUIManager.hpp

#ifndef GUIMANAGER_HPP_INCLUDED
#define GUIMANAGER_HPP_INCLUDED

#include "GUIWidget.hpp"

namespace summerproject
{
	class DrawManager;

	class GUIManager
	{
	public:
		GUIManager();
		~GUIManager();

		bool Initialize(const std::string& filename);
		void Shutdown();

		void Update();
		void Draw(DrawManager* draw_manager);

		void AttachWidget(GUIWidget* widget);

	private:
		void OnMouse(const MouseEvent& event);
		void OnChange(const GUIWidget* widget);

	private:
		bool m_dirty;
		sf::VertexArray m_vertex_array;
		sf::Texture* m_texture;
		std::vector<GUIWidget*> m_widgets;
	};
} // namespace summerproject

#endif // GUIMANAGER_HPP_INCLUDED
