#pragma once
#include "AbstractComponent.hpp"
#include <SFML\Graphics\Sprite.hpp>
namespace summerproject
{
	class Entity
	{
	public:
		Entity();
		virtual void Update(float deltatime) = 0;

		bool isActive();

		sf::Sprite* getSprite() const;
		sf::Vector2f getDirection() const;
		float getSpeed() const;
		/*void setActive(bool active);*/
		bool IsVisible();
		void setInvisible();

		void setDirection(float x, float y);
		void setDirection(const sf::Vector2f& direction);
		void setSpeed(float speed);
		enum Positions{ top, mid, low };
		void setVisible(bool visible);
		void setActive(bool active);

		/*virtual void onActivate();
		virtual void onDeactivate();*/

	protected:
		std::unique_ptr<sf::Sprite> m_sprite;
		sf::Vector2f m_direction;
		float m_speed;
		bool m_active;
		bool m_visible;

	};
};
