// VisualComponent.hpp

#ifndef VISUALCOMPONENT_HPP_INCLUDED
#define VISUALCOMPONENT_HPP_INCLUDED

#include "AbstractComponent.hpp"

namespace summerproject
{
	class VisualComponent : public AbstractComponent 
	{
	public:
		VisualComponent(const sf::Texture& texture);
		~VisualComponent();

		EComponentType GetType() const;

		const sf::Sprite& GetSprite() const;

	private:
		sf::Sprite m_sprite;
	};
}

#endif // VISUALCOMPONENT_HPP_INCLUDED
