// Action.hpp

#ifndef ACTION_HPP_INCLUDED
#define ACTION_HPP_INCLUDED

namespace summerproject
{
	enum class EActionType : unsigned int
	{
		Left,
		Right,
		Up,
		Down,
		Fire,
		AltFire,
		Select1,
		Select2,
		Select3,
		Select4,
		Count,
	};
} // namespace summerproject

#endif // ACTION_HPP_INCLUDED
