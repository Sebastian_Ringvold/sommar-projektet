// InputManager.hpp

#ifndef INPUTMANAGER_HPP_INCLUDED
#define INPUTMANAGER_HPP_INCLUDED

#include "MouseEvent.hpp"

namespace summerproject
{
	class InputManager
	{
		// note(tommi): a private class for the input manager
		// used to bind methods for callbacks
		// this is called a functor, for this to function 
		// we need an object and a method pointer
		class AbstractActionCallback
		{
		public:
			virtual void OnAction(EActionType action, bool state) = 0;
			virtual bool IsObject(void*) = 0;
		};

		template <class T>
		class ActionCallback : public AbstractActionCallback
		{
			T* m_object;
			void(T::*m_method)(EActionType, bool);
		public:
			ActionCallback(T* object, void(T::*method)(EActionType action, bool state))
				: m_object(object)
				, m_method(method)
			{
			}

			void OnAction(EActionType action, bool state)
			{
				// note(tommi): call the method on the specific method
				(*m_object.*m_method)(action, state);
			}

			bool IsObject(void* pointer)
			{
				return m_object == pointer;
			}
		};

		typedef std::vector<AbstractActionCallback*> ActionCallbackArray;

		// note(tommi): same as above, but for mouse events instead
		// a different way of basically doing the same thing
		// but this time we do not abstract away the actual mouse 
		// as an input device
		class AbstractMouseEventCallback
		{
		public:
			virtual ~AbstractMouseEventCallback() {}
			virtual void OnEvent(const MouseEvent& event) = 0;
			virtual bool IsObject(void* pointer) = 0;
		};

		template <class T>
		class MouseEventCallback : public AbstractMouseEventCallback
		{
			T* m_object;
			void(T::*m_method)(const MouseEvent& event);
		public:
			MouseEventCallback(T* object, void(T::*method)(const MouseEvent& event))
				: m_object(object)
				, m_method(method)
			{
			}

			void OnEvent(const MouseEvent& event)
			{
				(*m_object.*m_method)(event);
			}

			bool IsObject(void* pointer)
			{
				return m_object == pointer;
			}
		};

		typedef std::vector<AbstractMouseEventCallback*> MouseCallbackArray;

		// note(tommi): making the copy constructor and 
		// assignment operator private we make the class
		// non-copyable 
		InputManager(const InputManager&);
		InputManager& operator=(const InputManager&);

		friend class Engine;

	public:
		InputManager();
		~InputManager();

		bool Initialize();
		void Shutdown();

		void MapKeyToAction(sf::Keyboard::Key key, EActionType action);
		void UnregisterKeyActionListener(EActionType, void* object);

		template <class T> 
		void RegisterKeyActionListener(EActionType action, T* object, void(T::*method)(EActionType, bool))
		{
			m_actioncallbacks[action].push_back(new ActionCallback<T>(object, method));
		}

		void UnregisterMouseEventListener(void* object);
		template <class T>
		void RegisterMouseEventListener(T* object, void(T::*method)(const MouseEvent& event))
		{
			m_mouse_callbacks.push_back(new MouseEventCallback<T>(object, method));
		}

	private:
		void OnKeyboard(sf::Keyboard::Key key, bool state);
		void OnMouse(unsigned short x, unsigned short y, EMouseButton button, bool state);

	private:
		std::map<sf::Keyboard::Key, EActionType> m_keymapping;
		std::map<EActionType, ActionCallbackArray> m_actioncallbacks;

		MouseCallbackArray m_mouse_callbacks;
	};
} // namespace summerproject

#endif // INPUTMANAGER_HPP_INCLUDED
