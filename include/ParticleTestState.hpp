// ParticleTestState.hpp

#ifndef PARTICLETESTSTATE_HPP_INCLUDED
#define PARTICLETESTSTATE_HPP_INCLUDED

#include "AbstractState.hpp"

namespace summerproject
{
	class ParticleEffectManager;

	class ParticleTestState : public AbstractState
	{
	public:
		ParticleTestState();
		~ParticleTestState();

		bool Enter();
		void Exit();
		bool Update(float deltatime);
		void Draw();
		std::string GetNextState();

	private:
		ParticleEffectManager* m_particle_effect_manager;
	};
} // namespace summerproject

#endif // PARTICLETESTSTATE_HPP_INCLUDED
