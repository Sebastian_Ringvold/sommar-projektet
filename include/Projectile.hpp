#pragma once

#include "Entity.hpp"

namespace summerproject
{

class Projectile : public Entity
{
public:
	Projectile(const sf::Vector2f& position);
	~Projectile();

	void Update(float deltatime);
	float GetX();
	float GetY();
	bool IsActive();

	float GetTimer();
	void SetInvisible();

protected:
	float m_timer;

	bool m_visible;
	bool m_active;

	float m_spawntimer;
};
}