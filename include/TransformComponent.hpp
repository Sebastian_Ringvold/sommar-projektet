// TransformComponent.hpp

#ifndef TRANSFORMCOMPONENT_HPP_INCLUDED
#define TRANSFORMCOMPONENT_HPP_INCLUDED

#include "AbstractComponent.hpp"

namespace summerproject
{
	class TransformComponent : public AbstractComponent
	{
	public:
		TransformComponent();
		~TransformComponent();

		EComponentType GetType() const;

		const sf::Transform& GetTransform() const;
		void SetPosition(const sf::Vector2f& position);

	private:
		sf::Transformable m_transformable;
	};
}

#endif // TRANSFORMCOMPONENT_HPP_INCLUDED
