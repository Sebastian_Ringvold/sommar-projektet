// GUILabel.hpp

#ifndef GUILABEL_HPP_INCLUDED
#define GUILABEL_HPP_INCLUDED

#include "GUIWidget.hpp"

namespace summerproject
{
	class GUILabel : public GUIWidget
	{
	public:
		GUILabel(sf::Font* font);
		virtual ~GUILabel();

		virtual void OnDraw(sf::VertexArray& vertex_array);

		void SetTextString(const std::string& text);
		void SetTextColor(const sf::Color& color);
		void SetTextSize(unsigned int size);
		void SetTextPosition(float x, float y);
		void SetTextPosition(const sf::Vector2f& position);

		const sf::Text& GetText() const;

	private:
		sf::Text m_text;
	};
} // namespace summerproject

#endif // GUILABEL_HPP_INCLUDED
