// DrawManager.hpp

#ifndef DRAWMANAGER_HPP_INCLUDED
#define DRAWMANAGER_HPP_INCLUDED

namespace summerproject
{
	class DrawManager
	{
		// note(tommi): making the copy constructor and 
		// assignment operator private we make the class
		// non-copyable
		DrawManager(const DrawManager&);
		DrawManager& operator=(const DrawManager&);

	public:
		DrawManager(sf::RenderWindow* window);
		~DrawManager();

		bool Initialize();
		void Shutdown();

		void Draw(const sf::Drawable& drawable, const sf::RenderStates& states = sf::RenderStates::Default);

	private:
		sf::RenderWindow* m_window;
	};
} // namespace summerproject

#endif // DRAWMANAGER_HPP_INCLUDED
