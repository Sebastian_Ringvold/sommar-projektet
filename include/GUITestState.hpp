// GUITestState.hpp

#ifndef GUITESTSTATE_HPP_INCLUDED
#define GUITESTSTATE_HPP_INCLUDED

#include "AbstractState.hpp"

namespace summerproject
{
	class DrawManager;
	class GUIManager;

	class GUITestState : public AbstractState
	{
	public:
		GUITestState();
		~GUITestState();

		bool Enter();
		void Exit();
		bool Update(float deltatime);
		void Draw();
		std::string GetNextState();

	private:
		void OnAction(EActionType action, bool state);
		void OnMouse(const MouseEvent& event);

	private:
		DrawManager* m_draw_manager;
		GUIManager* m_gui_manager;
	};
} // namespace summerproject

#endif // GUITESTSTATE_HPP_INCLUDED
