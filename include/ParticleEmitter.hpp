// ParticleEmitter.hpp

#ifndef PARTICLEEMITTER_HPP_INCLUDED
#define PARTICLEEMITTER_HPP_INCLUDED

namespace summerproject
{
	class ParticleEmitter
	{
	public:
		ParticleEmitter();
		virtual ~ParticleEmitter();

		virtual void Update(float deltatime) = 0;
		virtual void Draw() = 0;

		const sf::Vector2f& GetPosition() const;
		void SetPosition(const sf::Vector2f& position);

	protected:
		sf::Vector2f m_position;
		sf::Texture* m_texture;
	};
} // namespace summerproject

#endif // PARTICLEEMITTER_HPP_INCLUDED
