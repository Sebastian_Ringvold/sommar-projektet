// PlayerControllerComponent.hpp

#ifndef PLAYERCONTROLLERCOMPONENT_HPP_INCLUDED
#define PLAYERCONTROLLERCOMPONENT_HPP_INCLUDED

#include "AbstractComponent.hpp"

namespace summerproject
{
	class PlayerControllerComponent : public AbstractComponent
	{
	public:
		PlayerControllerComponent();
		~PlayerControllerComponent();

		EComponentType GetType() const;

	private:
		void OnAction(EActionType type, bool state);

	private:
		bool m_actions[(unsigned int)EActionType::Count];
	};
} // namespace summerproject

#endif // PLAYERCONTROLLERCOMPONENT_HPP_INCLUDED
