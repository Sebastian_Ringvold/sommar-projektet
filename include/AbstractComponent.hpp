// AbstractComponent.hpp

#ifndef ABSTRACTCOMPONENT_HPP_INCLUDED
#define ABSTRACTCOMPONENT_HPP_INCLUDED

namespace summerproject
{
	class Entity;

	enum class EComponentType : unsigned int
	{
		TransformComponent,
		VisualComponent,
		CollisionComponent,
		PhysicsComponent,
		PlayerControllerComponent,
		Count,
	};

	class AbstractComponent
	{
	public:
		virtual ~AbstractComponent();
		virtual EComponentType GetType() const = 0;
		//virtual const Entity* GetParent() const = 0;
	};
} // namespace summerproject

#endif // ABSTRACTCOMPONENT_HPP_INCLUDED
